import sys,os

from pyRGA_convert_single import pyRGA__load_Specific_Scan


def numberOfScansInFile(inputFile):
	infile  =   open(inputFile,"r")
	startSplitting=False

	numScans=0

	for line in infile:
		line = line.strip() #Remove CR & LF
		element = line.split('\t')  #Split the line into a list of tab-delimited elements
				
		# Once we hit the column labels for the measurement data:
		if element[0].strip('"')=='Time':
			#Make a note that all following lines read from the file will be measurement data
			startSplitting=True 
			numDataColumns=len(element)
		
		# When we have reached the measurement data:
		elif startSplitting==True:
			if len(element)==numDataColumns:#Simple check to make sure this is really data
				numScans+=1

	return numScans

def getFileInfo(inputFile):
	infile  =   open(inputFile,"r")
	startSplitting=False
	numScans=0
	Mass = []
	Intensity = []
	timeStamps = []

	for line in infile:
		line = line.strip() #Remove CR & LF
		element = line.split('\t')  #Split the line into a list of tab-delimited elements

		if element[0]=='"Accuracy"':
			Accuracy=element[1].strip('"')

		if element[0]=='"Filament"':
			FilamentNumber=element[1].strip('"')	

		# Once we hit the column labels for the measurement data:
		if element[0].strip('"')=='Time':
			#Make a note that all following lines read from the file will be measurement data
			startSplitting=True 
			numDataColumns=len(element)

			# The mass values are stored in the column headers as "Mass 4.34" etc.
			# To extract the masses, we take each such header string and split it by spaces (i.e. --> "Mass","4.34")
			for ii in range(len(element)): #First two are timestamp & scan number, last one is summed mass
				temp=element[ii]
				templist=temp.split(' ')

				if templist[0].strip('"')=='Mass':
					if len(Mass)==0:
						firstDataColumnIndex=ii
					Mass.append(float(templist[1].strip('"')))
					Intensity.append(0) #Initialize the length of the Intensity array at the same time
					lastDataColumnIndex=ii		
		# When we have reached the measurement data:
		elif startSplitting==True:
			if len(element)==numDataColumns:#Simple check to make sure this is really data
				numScans+=1

				timeStamps.append(element[0].strip('"'))
				LastTimeStamp=element[0].strip('"')
				integerPressureSum=0
				
				for ii in range(firstDataColumnIndex,lastDataColumnIndex+1):
					Intensity[ii-firstDataColumnIndex]+=float(element[ii])
					if Mass[ii-firstDataColumnIndex]%1==0:
						integerPressureSum+=float(element[ii])*1.33 #1.33 for Torr to mBar
						
				LastPressure=integerPressureSum
				
				if numScans==1:
					FirstTimeStamp=element[0].strip('"')
					FirstPressure=integerPressureSum	

	return Accuracy,FilamentNumber,FirstTimeStamp,FirstPressure,timeStamps


if __name__ == '__main__':

	try:
		inputFile=sys.argv[1]
		outputFile=sys.argv[2]
	except:
		print("\nMissing argument\n\nExpected usage is pyRGA_convert_all.py <inputFileName> <outputFileName>")
		print("\tExample: pyRGA_convert_all.py example.txt reformattedData.txt")		
		sys.exit(1)

	if(os.path.isfile(inputFile)==False):
		print("\n I don't see a file named",inputFile)
		sys.exit(1)

	try:
		fp=open(inputFile,"a")
		fp.close()
	except:
		print("\nProblem opening the input file. Does it exist? Does the filename have tricky characters like spaces?")
		sys.exit(1)	

	if(os.path.isfile(outputFile)==True):
		print("\n>> Warning! << This outfile file already exists, so it will be overwritten.")
		input("If that is not what you want, abort with ctrl+c. Otherwise, push any key\n")	 

	print("There are",numberOfScansInFile(inputFile),"scans in this file. They will be saved as separate columns. Working on it...")

	# Initialize a list (to become a list of scans)
	Scan=[]
	# Suck out each scan from the file one at a time:
	for scanNumber in range(numberOfScansInFile(inputFile)):	
		Mass,Intensity=pyRGA__load_Specific_Scan(inputFile,whichScan=scanNumber)
		Scan.append(Intensity)

	fp=open(outputFile,"w")
	Accuracy,FilamentNumber,FirstTimeStamp,FirstPressure,timeStamps=getFileInfo(inputFile)

	#Write the file header
	fp.write("Original file\t"+str(inputFile))
	fp.write("\nAccuracy\t"+str(Accuracy))
	fp.write("\nFilament\t"+str(FilamentNumber))
	fp.write("\nFirst scan timestamp\t"+str(FirstTimeStamp))
	fp.write("\nFirst scan simple sum pressure (mBar)\t %.2e" % (FirstPressure))

	#Write column labels
	fp.write("\nMass")
	for scanNumber in range(numberOfScansInFile(inputFile)):
		fp.write("\ttimeStamp_"+timeStamps[scanNumber])

	for intensityIndex in range(len(Scan[0])):
		fp.write("\n"+str(Mass[intensityIndex]))
		for scanNumber in range(numberOfScansInFile(inputFile)):
			fp.write("\t"+str(Scan[scanNumber][intensityIndex]))
	fp.close()

	print("Conversion complete")