import sys,os


def pyRGA__load_Specific_Scan(inputFile,whichScan):
	"""Takes an MKS formatted txt file and returns the specified scan number (indexing starts from 0)"""    
 
	infile=open(inputFile,'r')
	startSplitting=False
	Mass = []
	Intensity = []
	scanIndex=0
	
	for line in infile:

		line = line.strip() #Remove CR & LF
		element = line.split('\t')  #Split the line into a list of tab-delimited elements


		# Once we hit the column labels for the measurement data:
		if element[0].strip('"')=='Time':
			#Make a note that all following lines read from the file will be measurement data
			startSplitting=True 
			numDataColumns=len(element)
			
			# The mass values are stored in the column headers as "Mass 4.34" etc.
			# To extract the masses, we take each such header string and split it by spaces (i.e. --> "Mass","4.34")
			for ii in range(len(element)): #First two are timestamp & scan number, last one is summed mass
				temp=element[ii]
				templist=temp.split(' ')

				if templist[0].strip('"')=='Mass':
					if len(Mass)==0:
						firstDataColumnIndex=ii
					Mass.append(float(templist[1].strip('"')))
					lastDataColumnIndex=ii
		
		# When we have reached the measurement data:
		elif startSplitting==True:

			if len(element)==numDataColumns:#Simple check to make sure this is really data 
				if scanIndex==int(whichScan):
					for ii in range(firstDataColumnIndex,lastDataColumnIndex+1):
						Intensity.append(float(element[ii])*1.33) #1.33 for Torr to mBar
			scanIndex+=1

	if len(Intensity)==0:
		print("WARNING! Null intensity data returned (Did you specify an out-of-range scan index?)")
	return Mass,Intensity



if __name__ == '__main__':

	try:
		inputFile=sys.argv[1]
		scanNumber=sys.argv[2]
		outputFile=sys.argv[3]
	except:
		print("\nMissing argument\n\nExpected usage is pyRGA_convert_single.py <inputFileName> <scanNumber> <outputFileName>")
		print("\tExample: pyRGA_convert_single.py example.txt 2 reformattedData.txt")		
		sys.exit(1)

	if(os.path.isfile(inputFile)==False):
		print("\n I don't see a file named",inputFile)
		sys.exit(1)

	try:
		fp=open(inputFile,"a")
		fp.close()
	except:
		print("\nProblem opening the input file. Does it exist? Does the filename have tricky characters like spaces?")
		sys.exit(1)	

	if(os.path.isfile(outputFile)==True):
		print("\n>> Warning! << This outfile file already exists, so it will be overwritten.")
		input("If that is not what you want, abort with ctrl+c. Otherwise, push any key\n")	 

	Mass,Intensity=pyRGA__load_Specific_Scan(inputFile,scanNumber)

	fp=open(outputFile,"w")
	fp.write("Mass\tIntensity\n")
	for index,element in enumerate(Mass):
		fp.write("{0}\t{1}\n".format(Mass[index],Intensity[index]))
	fp.close()

	print("Conversion complete")
