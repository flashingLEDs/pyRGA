# pyRGA

The software for MKS residual gas analyzers makes it possible to export mass 
specs as plaintext, but the formatting is a bit quirky - a scan of n (mass,intensity) pairs is saved as n columns rather than n rows. Also the mass values are saved as strings instead of just values, e.g. "Mass 0.56" instead of "0.56"

As a consequence, these files can't be directly loaded into e.g. Igor.

This set of python scripts will re-format an exported file if you want to look
at the data somewhere else. It can also plot data directly via matplotlib.

All of this assumes that you have already saved some RGA data somewhere, and used MKS's 
software to export it. It will look something like the file in this repository
called 'example.txt'

## Requirements:

To re-format saved data for analysis elsewhere: Python3

To use the Jupyter notebooks and plotting functions: Python3 + pylab
(I do this the easy way by just installing the anaconda suite (anaconda.com))

## Get basic information about a saved scan file

Command line input:

```	
python pyRGA_fileInfo.py exampleData/exampleData.txt
```

Output:

```	
Data in file: exampleData.txt
Measuring mass range 1.00 to 100.00 with accuracy 7 ,filament # 1
15 scan(s) in file
First scan started 11/8/2017 2:34:14 PM , sum pressure was 4.22e-09 mBar
Last scan started 11/8/2017 4:34:30 PM , sum pressure was 1.85e-09 mBar
```


## Re-exporting data

To export a specific scan from a saved file, use pyRGA_convert_single(inputFile, scanNumber, outputFile)
Example command line input:

```	
python pyRGA_convert_single.py exampleData/exampleData.txt 1 exampleData/output.txt
```

Example output file:
```	
Mass	Intensity
0.5	8.645665000000001e-13
0.56	1.0754380000000002e-12
0.63	1.0061583e-12
0.69	9.680937e-13
0.75	1.0475213000000001e-12
0.81	1.2792073e-12
0.88	1.1799893000000001e-12
etc
etc
```	

To export a ALL scans from a saved file, use pyRGA_convert_all(inputFile, outputFile). The n scans in the input file will be saved as n columns in the output file.

Example command line input:

```	
python pyRGA_convert_all.py exampleData/exampleData.txt exampleData/output.txt
```

Example of an output file:
```	
Original file	exampleData/exampleData.txt
Accuracy	7
Filament	1
First scan timestamp	19/6/2016 2:35:10 PM
First scan simple sum pressure (mBar)	 4.22e-09
Mass	Scan 0		Scan 1		Scan 2		Scan 3
0.5		9.85e-13	8.64e-13	7.25e-13	9.40e-13	
0.56	4.98e-13	1.07e-12	9.54e-13	1.03e-12	
etc
etc
```	


## Plotting

See the example jupyter notebook (example_pyRGA_notebook.ipynb). Gitlab.com will
render it in the browser non-interactively; to change it you must have jupyter
installed locally.