import sys,os

def pyRGA__printFileInfo(inputFile):
	infile  =   open(inputFile,"r")
	startSplitting=False
	Mass = []
	Intensity = []

	numScans=0

	for line in infile:
		line = line.strip() #Remove CR & LF
		element = line.split('\t')  #Split the line into a list of tab-delimited elements


		if element[0]=='"First mass"':
			FirstMass=element[1].strip('"')

		if element[0]=='"Last mass"':
			LastMass=element[1].strip('"')

		if element[0]=='"Accuracy"':
			Accuracy=element[1].strip('"')

		if element[0]=='"Filament"':
			FilamentNumber=element[1].strip('"')

				
		# Once we hit the column labels for the measurement data:
		if element[0].strip('"')=='Time':
			#Make a note that all following lines read from the file will be measurement data
			startSplitting=True 
			numDataColumns=len(element)

			# The mass values are stored in the column headers as "Mass 4.34" etc.
			# To extract the masses, we take each such header string and split it by spaces (i.e. --> "Mass","4.34")
			for ii in range(len(element)): #First two are timestamp & scan number, last one is summed mass
				temp=element[ii]
				templist=temp.split(' ')

				if templist[0].strip('"')=='Mass':
					if len(Mass)==0:
						firstDataColumnIndex=ii
					Mass.append(float(templist[1].strip('"')))
					Intensity.append(0) #Initialize the length of the Intensity array at the same time
					lastDataColumnIndex=ii
		
		# When we have reached the measurement data:
		elif startSplitting==True:
			if len(element)==numDataColumns:#Simple check to make sure this is really data
				numScans+=1

				LastTimeStamp=element[0].strip('"')
				integerPressureSum=0
				
				for ii in range(firstDataColumnIndex,lastDataColumnIndex+1):
					Intensity[ii-firstDataColumnIndex]+=float(element[ii])
					if Mass[ii-firstDataColumnIndex]%1==0:
						integerPressureSum+=float(element[ii])*1.33 #1.33 for Torr to mBar
						
				LastPressure=integerPressureSum
				
				if numScans==1:
					FirstTimeStamp=element[0].strip('"')
					FirstPressure=integerPressureSum
				
	print("Data in file:",inputFile)
	print("Measuring mass range",FirstMass,"to",LastMass,"with accuracy",Accuracy,",filament #",FilamentNumber)
	print(numScans,"scan(s) in file")
	
	print("First scan started",FirstTimeStamp,", simple sum pressure was %.2e mBar" % (FirstPressure) )
	
	if numScans>1:
		print("Last scan started",LastTimeStamp,", simple sum pressure was %.2e mBar" % (LastPressure) )   


if __name__ == '__main__':

	try:
		inputFile=sys.argv[1]
	except:
		print("\nMissing argument\n\nExpected usage is pyRGA_fileInfo.py <inputFileName>")
		print("\tExample: pyRGA_fileInfo.py example.txt")      
		sys.exit(1)

	if(os.path.isfile(inputFile)==False):
		print("\n I don't see a file named",inputFile)
		sys.exit(1)
	try:
		fp=open(inputFile,"a")
		fp.close()
	except:
		print("\nProblem opening the input file. Does it exist? Does the filename have tricky characters like spaces?")
		sys.exit(1) 

	pyRGA__printFileInfo(inputFile)

